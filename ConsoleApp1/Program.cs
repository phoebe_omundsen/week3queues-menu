﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static Queue userQueue = new Queue();

        static void Main(string[] args)
        {
            bool exitMenu = false;
            while (!exitMenu)
            {
                Console.Clear();
                Console.WriteLine();
                Console.WriteLine("||~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~||");
                Console.WriteLine("||- - - - - - - - - - - - - - - M E N U - - - - - - - - - - - - - - -||");
                Console.WriteLine("||~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~||");
                Console.WriteLine("||                                                                   ||");
                Console.WriteLine("||                            1. ADD ITEM                            ||");
                Console.WriteLine("||                                                                   ||");
                Console.WriteLine("||                           2. DELETE ITEM                          ||");
                Console.WriteLine("||                                                                   ||");
                Console.WriteLine("||                     3. NUMBER OF ITEMS IN QUEUE                   ||");
                Console.WriteLine("||                                                                   ||");
                Console.WriteLine("||                           4. FIND AN ITEM                         ||");
                Console.WriteLine("||                                                                   ||");
                Console.WriteLine("||                         5. DISPLAY ALL ITEMS                      ||");
                Console.WriteLine("||                                                                   ||");
                Console.WriteLine("||                               6. EXIT                             ||");
                Console.WriteLine("||                                                                   ||");
                Console.WriteLine("||~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~||");
                Console.WriteLine("||- - - - - -  PLEASE SELECT A MENU OPTION AND HIT ENTER  - - - - - -||");
                Console.WriteLine("||~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~||");
                Console.Write("                              MENU OPTION = ");

                var userInput = Console.ReadLine();
                Console.Clear();

                switch (userInput)
                {
                    case "1":
                        Console.WriteLine("~ ~ ~ ~ ~ ~ ~ ~ You have selected option 1 ~ ~ ~ ~ ~ ~ ~ ~ ");
                        Console.Write("Please enter an item to be added to the Queue: ");
                        AddItem();
                        break;
                    case "2":
                        Console.WriteLine("~ ~ ~ ~ ~ ~ ~ ~ You have selected option 2 ~ ~ ~ ~ ~ ~ ~ ~ ");
                        Console.WriteLine("The first item added to your queue will now be deleted. Press Any Key to return to the Menu.");
                        DeleteItem();
                        break;
                    case "3":
                        Console.WriteLine("~ ~ ~ ~ ~ ~ ~ ~ You have selected option 3 ~ ~ ~ ~ ~ ~ ~ ~ ");
                        Console.Write("The amount of items in the Queue is currenty: ");
                        CountItems();
                        break;
                    case "4":
                        Console.WriteLine("~ ~ ~ ~ ~ ~ ~ ~ You have selected option 4 ~ ~ ~ ~ ~ ~ ~ ~ ");
                        Console.Write("Please enter the item you wish to find conatined in the queue: ");
                        FindItem();
                        break;
                    case "5":
                        Console.WriteLine("~ ~ ~ ~ ~ ~ ~ ~ You have selected option 5 ~ ~ ~ ~ ~ ~ ~ ~ ");
                        Console.WriteLine("You have selected option 5 - All of the items in the queue will be listed : ");
                        ListItems();
                        Console.ReadKey();
                        break;
                    case "6":
                        Console.WriteLine("~ ~ ~ ~ ~ ~ ~ ~ You have selected option 6 ~ ~ ~ ~ ~ ~ ~ ~ ");
                        Console.WriteLine("You have chosen to exit the applicaton. Goodbye :) ");
                        exitMenu = true;
                        break;
                    default:
                        Console.WriteLine("~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ Computer says Nooooooo ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~");
                        Console.WriteLine("Inavlid selection. Please enter an option between 1 - 6. Press Any Key to try again");
                        Console.ReadKey();
                        break;

                }  

            }
            Console.ReadKey();
        }

        static void AddItem()
        {
            var case1 = Console.ReadLine();
            userQueue.Enqueue(case1);

        }

        static void DeleteItem()
        {
            if (userQueue.Count != 0)
            {
                userQueue.Dequeue();
            }
            else
            {
                Console.WriteLine("queue is already empty");
            }
            Console.ReadKey();
        }

        static void CountItems()
        {
            var countQueue = userQueue.Count;
            Console.Write(countQueue);
        }

        static void FindItem()
        {
            var userInput2 = Console.ReadLine();
            var containsValue = userQueue.Contains(userInput2);

            if (containsValue != false)
            {
                Console.WriteLine("The item you have entered was found in the queue :) ");
            }
            else
                Console.WriteLine("Sorry the item you entered was not able to found within the queue :( ");
        }

        static void ListItems()
        {
            string[] itemList = new string[userQueue.Count];
            userQueue.CopyTo(itemList, 0);

            foreach (string i in itemList)
            {
                Console.WriteLine(i);
            }
        }
    }
}
